# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170104031653) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string   "name"
    t.string   "province"
    t.string   "city"
    t.string   "district"
    t.string   "full_street"
    t.string   "phone"
    t.boolean  "is_default",  default: false
    t.integer  "customer_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["customer_id"], name: "index_addresses_on_customer_id", using: :btree
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "slug"
    t.string   "ancestry"
    t.integer  "ancestry_depth"
    t.index ["ancestry"], name: "index_categories_on_ancestry", using: :btree
    t.index ["ancestry_depth"], name: "index_categories_on_ancestry_depth", using: :btree
    t.index ["name"], name: "index_categories_on_name", using: :btree
    t.index ["slug"], name: "index_categories_on_slug", using: :btree
  end

  create_table "custom_windows", force: :cascade do |t|
    t.string   "link"
    t.string   "img"
    t.integer  "position"
    t.boolean  "displaying"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customers", force: :cascade do |t|
    t.string   "email"
    t.string   "name"
    t.string   "password_digest"
    t.string   "remember_token"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["email"], name: "index_customers_on_email", using: :btree
  end

  create_table "name_stats", force: :cascade do |t|
    t.integer "name_id"
    t.integer "year"
    t.string  "state"
    t.integer "count"
  end

  create_table "options", force: :cascade do |t|
    t.string   "name"
    t.integer  "product_id"
    t.integer  "position"
    t.text     "values"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_options_on_product_id", using: :btree
  end

  create_table "pictures", force: :cascade do |t|
    t.string   "pic"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_images", force: :cascade do |t|
    t.integer  "product_id"
    t.string   "img"
    t.integer  "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["position"], name: "index_product_images_on_position", using: :btree
    t.index ["product_id"], name: "index_product_images_on_product_id", using: :btree
  end

  create_table "products", force: :cascade do |t|
    t.string   "title"
    t.text     "body_html"
    t.datetime "published_at"
    t.string   "vendor"
    t.text     "keywords"
    t.decimal  "price",        precision: 8, scale: 2
    t.integer  "category_id"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.string   "slug"
    t.datetime "deleted_at"
    t.integer  "stock_qty",                            default: 0
    t.integer  "status",                               default: 0
    t.boolean  "hot_sale",                             default: false
    t.boolean  "new_arrival",                          default: true
    t.string   "cover"
    t.index ["category_id"], name: "index_products_on_category_id", using: :btree
    t.index ["deleted_at"], name: "index_products_on_deleted_at", using: :btree
  end

  create_table "suggestions", force: :cascade do |t|
    t.string   "word"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["word"], name: "index_suggestions_on_word", using: :btree
  end

  create_table "taggings", id: false, force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "tag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_taggings_on_product_id", using: :btree
    t.index ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
  end

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "taggings_count", default: 0
  end

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "name"
    t.string   "password_digest"
    t.string   "remember_token"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "admin",           default: false
    t.index ["email"], name: "index_users_on_email", using: :btree
  end

  create_table "variants", force: :cascade do |t|
    t.decimal  "price",      precision: 8, scale: 2
    t.string   "sku"
    t.integer  "stock"
    t.integer  "position"
    t.integer  "product_id"
    t.string   "option1"
    t.string   "option2"
    t.string   "option3"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_variants_on_deleted_at", using: :btree
    t.index ["product_id"], name: "index_variants_on_product_id", using: :btree
  end

  create_table "wish_lists", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "customer_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["customer_id"], name: "index_wish_lists_on_customer_id", using: :btree
    t.index ["product_id"], name: "index_wish_lists_on_product_id", using: :btree
  end

end
