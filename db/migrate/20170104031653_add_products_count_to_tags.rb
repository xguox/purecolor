class AddProductsCountToTags < ActiveRecord::Migration[5.0]
  def change
    add_column :tags, :taggings_count, :integer, default: 0

    Tag.pluck(:id).each do |i|
      Tag.reset_counters(i, :taggings)
    end
  end
end
