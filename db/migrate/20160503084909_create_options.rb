class CreateOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :options do |t|
      t.string :name
      t.integer :product_id
      t.integer :position
      t.text    :values

      t.timestamps
    end

    add_index :options, :product_id
  end
end
