class AddAncestryToCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :slug, :string
    add_column :categories, :ancestry, :string
    add_column :categories, :ancestry_depth, :integer

    add_index  :categories, :slug
    add_index  :categories, :ancestry
    add_index  :categories, :ancestry_depth
  end
end
