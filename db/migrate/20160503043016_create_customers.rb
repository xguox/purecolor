class CreateCustomers < ActiveRecord::Migration[5.0]
  def change
    create_table :customers do |t|
      t.string :email, index: true
      t.string :name
      t.string :password_digest
      t.string :remember_token

      t.timestamps
    end
  end
end
