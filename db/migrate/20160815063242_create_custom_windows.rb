class CreateCustomWindows < ActiveRecord::Migration[5.0]
  def change
    create_table :custom_windows do |t|
      t.string    :link
      t.string    :img
      t.integer   :position
      t.boolean   :displaying

      t.timestamps
    end
  end
end
