class CreateAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :addresses do |t|
      t.string :name
      t.string :province
      t.string :city
      t.string :district
      t.string :full_street
      t.string :phone
      t.boolean    :is_default, default: false
      t.references :customer

      t.timestamps
    end
  end
end
