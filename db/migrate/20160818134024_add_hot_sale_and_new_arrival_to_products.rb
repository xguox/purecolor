class AddHotSaleAndNewArrivalToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :hot_sale, :boolean, default: false
    add_column :products, :new_arrival, :boolean, default: true
  end
end
