class AddDeletedAtToProductsAndVariants < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :deleted_at, :datetime
    add_column :products, :stock_qty, :integer, default: 0
    add_column :products, :status, :integer, default: 0

    add_column :variants, :deleted_at, :datetime

    add_index :products, :deleted_at
    add_index :variants, :deleted_at
  end
end
