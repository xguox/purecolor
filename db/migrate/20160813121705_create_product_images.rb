class CreateProductImages < ActiveRecord::Migration[5.0]
  def change
    create_table :product_images do |t|
      t.references(:product)
      t.string   :img
      t.integer  :position, index: true
      t.timestamps
    end
  end
end
