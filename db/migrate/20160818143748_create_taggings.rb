class CreateTaggings < ActiveRecord::Migration[5.0]
  def change
    create_table :taggings, id: false do |t|
      t.references(:product)
      t.references(:tag)
      t.timestamps
    end
  end
end
