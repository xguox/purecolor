class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :title
      t.text :body_html
      t.datetime :published_at
      t.string :vendor
      t.text  :keywords
      t.decimal :price, precision: 8, scale: 2
      t.references :category
      t.timestamps
    end
  end
end
