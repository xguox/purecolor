# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Category.create([{ name: '睡衣', slug: 'Pajamas', ancestry_depth: 0 }, { name: '文胸', slug: 'Bra', ancestry_depth: 0 }, { name: '上装', slug: 'Tops', ancestry_depth: 0 }, { name: '下装', slug: 'Bottoms', ancestry_depth: 0 }, { name: '裙装', slug: 'Dress', ancestry_depth: 0 }])