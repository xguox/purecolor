Rails.application.routes.draw do
  namespace :backend, path: "admin" do
    root 'pages#home'

    get 'sign_in', to: 'sessions#new', as: 'sign_in'
    post 'sign_in', to: 'sessions#create'
    delete 'sign_out', to: 'sessions#destroy', as: 'sign_out'

    resources :users
    resources :categories, except: :show
    resources :custom_windows
    resources :products do
      member do
        get :set_cover
      end

      resources :options do
        collection do
          put :update_all
        end
      end
      resources :variants
      resources :product_images
    end

    resources :pictures, only: :create
  end

  namespace :frontend, path: "shop" do
    root 'pages#home'

    get 'sign_in', to: 'sessions#new', as: 'sign_in'
    post 'sign_in', to: 'sessions#create'
    delete 'sign_out', to: 'sessions#destroy', as: 'sign_out'

    get 'settings', to: 'customers#edit', as: :settings

    resources :search, only: :index

    get 'p/:slug', to: 'products#show', as: :display_spu
    resources :products, only: [:index], param: :slug do
      # get '/content', to: 'products#content'
      # get '/basic', to: 'products#basic'

      collection do
        get 'new_arrival'
        get 'hot_sales'
      end
    end

    resources :customers
    resources :addresses

    resources :variants do
      collection do
        get  :get_selected
      end
    end

    resource :cart, only: [:show] do
      post 'add_to_cart', to: 'carts#add', as: :add_to
      post 'remove_from_cart/:variant_id', to: 'carts#remove', as: :remove_from
      post 'change_qty/:variant_id', to: 'carts#change_qty', as: :change_qty_in
    end

    get 'my_wish_list', to: 'wish_lists#my', as: :my_wish_list
    post '/wish/:product_id', to: 'wish_lists#add', as: :add_to_wish_list
    delete '/wish/:product_id', to: 'wish_lists#remove', as: :remove_from_wish_list

    resources :categories, only: :show, param: :slug do
      get 'tags/:tag_id', action: :tag, on: :member, as: :tag
    end
    resources :tags, only: :show
  end

  root 'frontend/pages#home'
end

