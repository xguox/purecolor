::CarrierWave.configure do |config|
  config.storage             = :qiniu
  config.qiniu_access_key    = ENV['QINIU_AK']
  config.qiniu_secret_key    = ENV['QINIU_SK']
  config.qiniu_bucket        = "do4fun"
  config.qiniu_bucket_domain = "do4fun.qiniudn.com"
  config.qiniu_block_size    = 4*1024*1024
  config.qiniu_protocal      = "http"
  config.qiniu_up_host       = 'http://up.qiniug.com'
end