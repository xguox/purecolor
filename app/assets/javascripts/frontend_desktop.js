// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery.ez-plus
//= require jquery.citys
//= require semantic-ui

$(document).ready(function() {
  $('#app .ui.sidebar')
    .sidebar({context:$('#app')})
    .sidebar('setting', 'transition', 'overlay')
    .sidebar('show');

  $('#product_show ul.radio')
    // redo every change
    .on('click', function () {
      // refresh "current classes"
      $('li', this).removeClass('selected')
        .filter(':has(:checked)').addClass('selected');
  });

  $('#big-img').ezPlus({
      gallery: 'imgs-gallery', cursor: 'pointer', galleryActiveClass: 'active',
      imageCrossfade: true
  });

  //pass the images to Fancybox
  $("#big-img").on("click", function(e) {
    var ez = $('#big-img').data('ezPlus');
    $.fancybox(ez.getGalleryList());
    return false;
  });
});