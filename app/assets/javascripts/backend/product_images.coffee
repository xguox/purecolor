$ ->
  $('#new_product_image').fileupload
    dataType: 'script'
    add: (e, data) ->
          types = /(\.|\/)(gif|jpe?g|png)$/i
          file = data.files[0]
          data.context = $(tmpl("template-upload", file))
          $('#new_product_image').append(data.context)
          data.submit()
        progress: (e, data) ->
          if data.context
            progress = parseInt(data.loaded / data.total * 100, 10)
            data.context.find('.bar').css('width', progress + '%')