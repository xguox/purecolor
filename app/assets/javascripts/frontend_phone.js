//= require jquery
//= require jquery_ujs
//= require jquery.ez-plus
//= require jquery.citys
//= require owl.carousel.min
//= require jquery.lazyload.min
//= require semantic-ui

$(document).ready(function(){
  $('.featured-owl').owlCarousel({items: 2, margin: 10, loop: true, stagePadding: 10, autoplay: true, lazyLoad: true});
  $('.product-imgs-owl').owlCarousel({items: 1, margin: 10, loop: true, stagePadding: 0, autoplay: true, lazyLoad: true});
  $("img.lazy").lazyload();

  // sidebar interact
  $('.ui.sidebar').first()
    .sidebar('attach events', '.launch.item')
  ;
  $('.launch.item')
    .removeClass('disabled')
  ;

});