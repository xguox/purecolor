module Frontend::VariantsHelper
  def get_cart_item_count(variant_id)
    Redis.current.hget(current_customer.cart_key, variant_id)
  end
end
