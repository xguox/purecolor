module Backend::ProductImagesHelper
  def highlight_default(product, image)
    if product.cover == image.img_url(:big)
      'yellow'
    end
  end
end
