module ApplicationHelper
  def active_page(path)
    "active" if current_page?(path)
  end

  def format_time(time)
    time.strftime("%Y-%m-%d")
  end

  def render_products_list_title
    if @category.present? && @tag.present?
      "##{@category.name}/#{@tag.name}#"
    elsif @category.present?
      "##{@category.name}#"
    elsif @tag.present?
      "##{@tag.name}#"
    else
      '#所有的纯色#'
    end
  end
end
