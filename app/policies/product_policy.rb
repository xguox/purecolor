class ProductPolicy < ApplicationPolicy
  attr_reader :user, :product

  def initialize(user, product)
    @user = user
    @product = product
  end

  def index?
    create?
  end

  def show?
    create?
  end

  def create?
    user.present?
  end

  def new?
    create?
  end

  def update?
    create?
  end

  def edit?
    update?
  end

  def destroy?
    create?
  end

  def update_all?
    create?
  end

  def set_cover?
    create?
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
