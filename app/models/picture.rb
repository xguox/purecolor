class Picture < ApplicationRecord
  mount_uploader :pic, BaseUploader
end

# == Schema Information
#
# Table name: pictures
#
#  id         :integer          not null, primary key
#  pic        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
