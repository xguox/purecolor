class Variant < ApplicationRecord
  belongs_to :product
  validates :price, presence: true
  scope :not_deleted, -> { where(deleted_at: nil) }
end

# == Schema Information
#
# Table name: variants
#
#  id         :integer          not null, primary key
#  price      :decimal(8, 2)
#  sku        :string
#  stock      :integer
#  position   :integer
#  product_id :integer
#  option1    :string
#  option2    :string
#  option3    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#
# Indexes
#
#  index_variants_on_deleted_at  (deleted_at)
#  index_variants_on_product_id  (product_id)
#
