require 'active_support/concern'

module ProductsSearchable
  extend ActiveSupport::Concern

  included do
    searchkick merge_mappings: false,
               index_name: -> { "q_products" },
               callbacks: :async,
               highlight: [:title],
               settings: {
                analysis: {
                  analyzer: {
                    ik_smart_html: {
                      type: 'custom',
                      char_filter: ['html_strip'],
                      tokenizer: 'ik_smart'
                    }
                  }
                }
               },
               mappings: {
               product: {
                  properties: {
                    title: {
                      type: 'text',
                      fields: {
                        analyzed: {
                          type: 'text',
                          analyzer: 'ik_smart',
                          boost: 8
                        }
                      }
                    },
                    keywords: {
                      type: 'text',
                      fields: {
                        analyzed: {
                          type: 'text',
                          analyzer: 'ik_smart',
                          boost: 5
                        }
                      }
                    },
                    body_html: {
                      type: 'text',
                      fields: {
                        analyzed: {
                          type: 'text',
                          analyzer: 'ik_smart_html'
                        }
                      }
                    }
                  }
                }
              }
  end

  def should_index?
    deleted_at.nil?
  end

  def search_data
    as_json(only: [:title, :keywords, :body_html])
  end

  class_methods do
    def custom_search(params)
      p = params[:page].to_i
      page = p == 0 ? 1 : p
      per_page = (params[:per_page] || 20).to_i
      query = Product.search(params[:q], page: page, per_page: per_page, execute: false)

      multi_match = {
                     multi_match: {
                      query:  params[:q],
                      fields: [ "title.analyzed", "body_html.analyzed", "keywords.analyzed" ]
                    }
                  }
      query.body[:query] = multi_match
      query.body[:highlight] = {
                                  fields: {
                                    "title.analyzed": {}
                                  }
                                }
      query.execute
    end
  end
end