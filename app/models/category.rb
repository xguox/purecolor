class Category < ApplicationRecord
  has_ancestry cache_depth: true
  has_many :products
  has_many :tags, through: :products

  validates_presence_of :name
  validates_presence_of :slug
  validates_uniqueness_of :slug

  def self.get_categories
    Rails.cache.fetch('top_categories') do
      where(ancestry_depth: 0).load
    end
  end

  # def self.get_cache_category(slug)
  #   Rails.cache.fetch("category_#{slug}") do
  #     find_by(slug: slug)
  #   end
  # end

  def r_set_tag_products_count(tag_id)
    # products_count = products.joins(:taggings).
    #                           where("taggings.tag_id = #{tag_id}").count
    tag_products_count = tags_products_count[tag_id]
    redis.zadd("category:#{id}", tag_products_count, tag_id)
  end

  # load top 5 tags
  def top_5_tags
    ids = redis.zrevrange("category:#{id}", 0, 4)
    Tag.find_ordered(ids)
    # .order("FIELD(id,#{ids.join(',')})") # only MySQL
  end

  def tag_products_count_map
    redis.zrevrange("category:#{id}", 0, 4, with_scores: true).to_h
  end

  def tags_products_count
    @tags_products_count ||= products.joins(:taggings)
                                     .group('taggings.tag_id')
                                     .count
  end

  def reset_tags_products_counts
    redis.del("category:#{id}")
    tags.reload.each { |t| r_set_tag_products_count(t.id) }
  end

  private

  def redis
    Redis.current
  end
end

# == Schema Information
#
# Table name: categories
#
#  id             :integer          not null, primary key
#  name           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  slug           :string
#  ancestry       :string
#  ancestry_depth :integer
#
# Indexes
#
#  index_categories_on_ancestry        (ancestry)
#  index_categories_on_ancestry_depth  (ancestry_depth)
#  index_categories_on_name            (name)
#  index_categories_on_slug            (slug)
#
