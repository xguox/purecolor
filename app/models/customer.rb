class Customer < ApplicationRecord
  has_secure_password

  has_many :addresses
  has_many :wish_lists
  has_many :wish_list_items, through: :wish_lists, class_name: 'Product'

  validates_presence_of :name
  validates_uniqueness_of :email
  validates :email, presence: true, uniqueness: { case_sensitive: false },
                    format: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i,
                    on: :create

  before_save { self.email = email.downcase }
  before_save :update_remember_token, if: :password_digest_changed?

  def remember
    return if remember_token.present?
    self.remember_token = SecureRandom.urlsafe_base64
    save
  end

  def update_remember_token
    self.remember_token = SecureRandom.urlsafe_base64
  end

  def wished?(product_id)
    WishList.exists?(customer_id: id, product_id: product_id)
  end

  def wish(product_id)
    if wished?(product_id)
      WishList.where(customer_id: id, product_id: product_id).delete_all
      redis.decr wish_list_items_count_key
      redis.decr "wished_by_count:#{id}"
    else
      WishList.create!(customer_id: id, product_id: product_id)
      redis.incr wish_list_items_count_key
      redis.incr "wished_by_count:#{id}"
    end
  end

  def wish_list_items_count
    redis.get(wish_list_items_count_key) || 0
  end

  def wish_list_items_count_key
    "wish_list_items_count:#{id}"
  end

  def cart_items
    ids = redis.hkeys(cart_key)
    Variant.where(id: ids)
  end

  def cart_items_count
    redis.hlen(cart_key) || 0
  end

  def cart_key
    "cart:#{id}"
  end

  private

  def redis
    Redis.current
  end
end

# == Schema Information
#
# Table name: customers
#
#  id              :integer          not null, primary key
#  email           :string
#  name            :string
#  password_digest :string
#  remember_token  :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_customers_on_email  (email)
#
