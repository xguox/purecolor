class Suggestion < ApplicationRecord
  def self.save_word(word)
    find_or_create_by(word: word)
  end
end

# == Schema Information
#
# Table name: suggestions
#
#  id         :integer          not null, primary key
#  word       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_suggestions_on_word  (word)
#
