class Option < ApplicationRecord
  belongs_to :product
  validates_associated :product

  # TODO: values uniq?
  serialize :values, Array
  validates :name, presence: true # , uniqueness: true
  validates_uniqueness_of :name, scope: :product_id
  validates :values, presence: true
end

# == Schema Information
#
# Table name: options
#
#  id         :integer          not null, primary key
#  name       :string
#  product_id :integer
#  position   :integer
#  values     :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_options_on_product_id  (product_id)
#
