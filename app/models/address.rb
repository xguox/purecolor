class Address < ApplicationRecord
  belongs_to :customer
  after_save :check_default

  private

  def check_default
    customer.addresses.where.not(id: id).update(is_default: false) if is_default
  end
end

# == Schema Information
#
# Table name: addresses
#
#  id          :integer          not null, primary key
#  name        :string
#  province    :string
#  city        :string
#  district    :string
#  full_street :string
#  phone       :string
#  is_default  :boolean          default(FALSE)
#  customer_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_addresses_on_customer_id  (customer_id)
#
