class ProductImage < ApplicationRecord
  belongs_to :product
  mount_uploader :img, ProductImageUploader

  before_create :set_position

  private

  def set_position
    max_pos = product.product_images.maximum(:position)
    self.position = if max_pos.nil?
                      0
                    else
                      max_pos + 1
                    end
  end
end

# == Schema Information
#
# Table name: product_images
#
#  id         :integer          not null, primary key
#  product_id :integer
#  img        :string
#  position   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_product_images_on_position    (position)
#  index_product_images_on_product_id  (product_id)
#
