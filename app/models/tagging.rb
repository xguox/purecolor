class Tagging < ApplicationRecord
  belongs_to :tag, counter_cache: true
  belongs_to :product

  after_save :refresh_category_counter
  after_create :incr_category_tag_count
  after_destroy :decr_category_tag_count

  private

  def incr_category_tag_count
    redis.zincrby("category:#{product.category_id}", 1, tag_id)
  end

  def decr_category_tag_count
    redis.zincrby("category:#{product.category_id}", -1, tag_id)
  end

  def refresh_category_counter
    product.category.r_set_tag_products_count(tag_id)
  end

  def redis
    Redis.current
  end
end

# == Schema Information
#
# Table name: taggings
#
#  product_id :integer
#  tag_id     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_taggings_on_product_id  (product_id)
#  index_taggings_on_tag_id      (tag_id)
#
