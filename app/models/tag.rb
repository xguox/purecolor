class Tag < ApplicationRecord
  has_many :taggings
  has_many :products, through: :taggings

  def products_count
    taggings_count
  end

  # tmp solution
  def self.find_ordered(ids)
    order_clause = 'CASE id '
    ids.each_with_index do |id, index|
      order_clause << sanitize_sql_array(['WHEN ? THEN ? ', id, index])
    end
    order_clause << sanitize_sql_array(['ELSE ? END', ids.length])
    where(id: ids).order(order_clause)
  end
end

# == Schema Information
#
# Table name: tags
#
#  id             :integer          not null, primary key
#  name           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  taggings_count :integer          default(0)
#
