class User < ApplicationRecord
  has_secure_password

  validates_presence_of :name
  validates :email, presence: true, uniqueness: { case_sensitive: false },
                    format: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i,
                    on: :create
  before_save { self.email = email.downcase }
  before_save :update_remember_token, if: :password_digest_changed?

  def remember
    return if remember_token.present?
    self.remember_token = SecureRandom.urlsafe_base64
    save
  end

  def update_remember_token
    self.remember_token = SecureRandom.urlsafe_base64
  end

  def admin?
    admin
  end
end

# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  email           :string
#  name            :string
#  password_digest :string
#  remember_token  :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  admin           :boolean          default(FALSE)
#
# Indexes
#
#  index_users_on_email  (email)
#
