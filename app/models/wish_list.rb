class WishList < ApplicationRecord
  belongs_to :wish_list_item, class_name: 'Product', foreign_key: 'product_id'
  belongs_to :customer
end

# == Schema Information
#
# Table name: wish_lists
#
#  id          :integer          not null, primary key
#  product_id  :integer
#  customer_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_wish_lists_on_customer_id  (customer_id)
#  index_wish_lists_on_product_id   (product_id)
#
