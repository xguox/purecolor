class CustomWindow < ApplicationRecord
  mount_uploader :img, ProductImageUploader

  after_save :expire_features

  def self.features
    Rails.cache.fetch('custom_window_features') do
      CustomWindow.where(displaying: true).order('position ASC').load
    end
  end

  def expire_features
    Rails.cache.delete('custom_window_features')
  end
end

# == Schema Information
#
# Table name: custom_windows
#
#  id         :integer          not null, primary key
#  link       :string
#  img        :string
#  position   :integer
#  displaying :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
