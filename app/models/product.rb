class Product < ApplicationRecord
  include ProductsSearchable

  belongs_to :category
  has_many :variants, dependent: :destroy
  has_many :options, dependent: :destroy
  has_many :product_images, dependent: :destroy
  has_many :taggings, dependent: :destroy
  has_many :tags, through: :taggings, dependent: :destroy

  # accepts_nested_attributes_for :options

  validates_length_of :options, maximum: 3
  validates :title, presence: true
  validates :price, presence: true

  before_create :init_slug

  delegate :name, to: :category, prefix: true
  scope :not_deleted, -> { where(deleted_at: nil) }
  scope :hot_sales,
        -> { where(deleted_at: nil, hot_sale: 1).order('created_at DESC') }
  scope :new_arrivals,
        -> { where(deleted_at: nil, new_arrival: 1).order('created_at DESC') }

  SOLD_OUT = 0
  ON_OFFER = 1

  STATUS = {
    SOLD_OUT => '下架',
    ON_OFFER => '在售'
  }

  def tag_list=(names)
    self.tags = names.map do |n|
      Tag.where(name: n.strip).first_or_create!
    end
  end

  def tag_list
    tags.pluck(:name)
  end

  def self.tagged_with(name)
    Tag.find_by!(name: name).products
  end

  def wished_by_count_key
    "wished_by_count:#{id}"
  end

  private

  def init_slug
    loop do
      slug = SecureRandom.urlsafe_base64
      break self.slug = slug unless Product.exists?(slug: slug)
    end
  end
end

# == Schema Information
#
# Table name: products
#
#  id           :integer          not null, primary key
#  title        :string
#  body_html    :text
#  published_at :datetime
#  vendor       :string
#  keywords     :text
#  price        :decimal(8, 2)
#  category_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  slug         :string
#  deleted_at   :datetime
#  stock_qty    :integer          default(0)
#  status       :integer          default(0)
#  hot_sale     :boolean          default(FALSE)
#  new_arrival  :boolean          default(TRUE)
#  cover        :string
#
# Indexes
#
#  index_products_on_category_id  (category_id)
#  index_products_on_deleted_at   (deleted_at)
#
