class VariantJob < ApplicationJob
  queue_as :default

  def perform(product_id)
    product = Product.find(product_id)
    options = product.options
    price = product.price || 10
    options[0].values.each do |o1_v|
      if options[1].present?
        options[1].values.each do |o2_v|
          if options[2].present?
            options[2].values.each do |o3_v|
              # create sku with 3 options
              v = product.variants.create(price: price, option1: o1_v, option2: o2_v, option3: o3_v)
            end
          else
            # create_sku  with 2 options
            v = product.variants.create(price: price, option1: o1_v, option2: o2_v)
          end
        end
      else
        # create sku only one option
        v = product.variants.create(price: price, option1: o1_v)

      end
    end

    Sidekiq.logger.info "#{product.title} ***** options #{options.map(&:id).join("-")}"
  end
end
