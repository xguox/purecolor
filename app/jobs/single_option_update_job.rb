class SingleOptionUpdateJob < ApplicationJob
  queue_as :default

  def perform(removed, added, option_id)
    option = Option.find option_id
    product = option.product
    options = product.options
    price   = product.price

    product.variants.where("option#{option.position}" => removed).update_all(deleted_at: Time.now)

    if option.position == 1
      added.each do |o1_v|
        if options[1].present?
          options[1].values.each do |o2_v|
            if option[2].present?
              option[2].values.each do |o3_v|
                product.variants.create(price: price, option1: o1_v, option2: o2_v, option3: o3_v)
              end
            else
              product.variants.create(price: price, option1: o1_v, option2: o2_v)
            end
          end
        else
          product.variants.create(price: price, option1: o1_v)
        end
      end
      return
    end

    if option.position == 2
      options[0].values.each do |o1_v|
        added.each do |o2_v|
          if options[3].present?
            options[3].values.each do |o3_v|
              product.variants.create(price: price, option1: o1_v, option2: o2_v, option3: o3_v)
            end
          else
            product.variants.create(price: price, option1: o1_v, option2: o2_v)
          end
        end
      end
      return
    end

    if option.position == 3
      options[0].values.each do |o1_v|
        options[1].values.each do |o2_v|
          added.each do |o3_v|
            product.variants.create(price: price, option1: o1_v, option2: o2_v, option3: o3_v)
          end
        end
      end
      return
    end
  end
end
