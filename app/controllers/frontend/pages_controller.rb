class Frontend::PagesController < Frontend::ApplicationController
  def home
    # CustomWindow
    @cw0 = CustomWindow.features[0]
    @cw1 = CustomWindow.features[1]
    @cw2 = CustomWindow.features[2]
    @cw3 = CustomWindow.features[3]
    @cw4 = CustomWindow.features[4]
    @cw5 = CustomWindow.features[5]
    @cw6 = CustomWindow.features[6]
    # products list
    @products = Product.not_deleted.new_arrivals.page(params[:page]).per(12)
  end
end
