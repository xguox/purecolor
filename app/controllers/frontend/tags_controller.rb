class Frontend::TagsController < Frontend::ApplicationController
  def show
    @tag = Tag.find(params[:id])
    @products = Product.tagged_with(@tag.name).not_deleted
                       .order('created_at DESC')
                       .page(params[:page]).per(12)

    render 'frontend/products/index'
  end
end
