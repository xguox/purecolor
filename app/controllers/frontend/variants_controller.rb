class Frontend::VariantsController < Frontend::ApplicationController

  def add_to_cart
  end

  def get_selected
    vals = params[:opts]
    product = Product.find params[:product_id]
    # options_count = product.options.count
    @variant = product.variants.where(option1: vals[0],
                                      option2: vals[1],
                                      option3: vals[2]).first
  end
end
