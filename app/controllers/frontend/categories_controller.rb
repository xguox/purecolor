class Frontend::CategoriesController < Frontend::ApplicationController
  def show
    @category = Category.find_by(slug: params[:slug])
    @products = @category.products.not_deleted
                         .order('created_at DESC')
                         .page(params[:page]).per(2)

    render 'frontend/products/index'
  end

  def tag
    @tag = Tag.find(params[:tag_id])
    @category = Category.find_by(slug: params[:slug])
    @products = Product.tagged_with(@tag.name).not_deleted
                       .where(category: @category)
                       .order('created_at DESC')
                       .page(params[:page]).per(12)

    render 'frontend/products/index'
  end
end
