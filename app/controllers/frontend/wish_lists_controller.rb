class Frontend::WishListsController < Frontend::ApplicationController
  skip_before_action :get_top_categories, only: [:add, :remove]
  before_action :current_customer_exists?

  def my
    @wish_lists = current_customer.wish_lists
  end

  def add
    @product = Product.find(params[:product_id])
    current_customer.wish params[:product_id]
    @current_count = current_customer.wish_list_items_count
  end

  def remove
    @product = Product.find(params[:product_id])
    current_customer.wish params[:product_id]
    @current_count = current_customer.wish_list_items_count
  end
end
