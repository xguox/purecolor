class Frontend::CartsController < Frontend::ApplicationController
  skip_before_action :get_top_categories, only: [:add, :remove, :change_qty]
  before_action :current_customer_exists?

  def show
    @cart_items = current_customer.cart_items
  end

  def add
    key = current_customer.cart_key
    variant_id = params[:variant_id]
    qty = params[:quantity]
    redis.hincrby key, variant_id, qty if variant_id.present?
  end

  def change_qty
    key = current_customer.cart_key
    variant_id = params[:variant_id]
    @variant = Variant.find variant_id
    qty = params[:quantity]
    redis.hset key, variant_id, qty if variant_id.present?
  end

  def remove
    redis.hdel current_customer.cart_key, params[:variant_id]
  end

  private

  def redis
    Redis.current
  end
end
