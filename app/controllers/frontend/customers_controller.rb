class Frontend::CustomersController < Frontend::ApplicationController
  def new
    @customer = Customer.new(customer_params)
  end

  def create
    @customer = Customer.new(customer_params)

    if @customer.save
      session[:customer_id] = @customer.id
      redirect_to root_path
    else
      flash[:notice] = @customer.errors.full_messages.join(' 。')
      redirect_to new_customer_path(customer: { name: customer_params[:name],
                                                email: customer_params[:email] })

    end
  end

  def edit

  end

  def update
    if current_customer && current_customer.authenticate(params[:customer][:old_password])
      if current_customer.update(customer_params)
        flash[:alert] = '密码更新成功, 请牢记, 谢谢.'
      else
        flash[:alert] = '确认密码不一致, 请重新输入.'
      end
    else
      flash[:alert] = '旧密码有误, 请重新输入'
    end
    redirect_to frontend_settings_path
  end

  private

  def customer_params
    params.require(:customer).permit(:email, :password, :password_confirmation, :name) if params[:customer]
  end
end
