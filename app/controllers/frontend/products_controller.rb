class Frontend::ProductsController < Frontend::ApplicationController
  def index
    render :index
  end

  def new_arrival
    @products = Product.new_arrivals.page(params[:page]).per(12)
    index
  end

  def hot_sales
    @products = Product.hot_sales.page(params[:page]).per(12)
    index
  end

  def show
    @product = Product.where(slug: params[:slug]).first
    @options = @product.options
    @no_footer           = true
    @no_top_menu         = true
    @no_top_search       = true
    @display_bottom_menu = true
  end

  # def content
  #   @product = Product.where(slug: params[:product_slug]).first
  #   @p_body = @product.body_html
  # end
end
