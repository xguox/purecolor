class Frontend::ApplicationController < ActionController::Base
  layout "application"
  protect_from_forgery with: :exception
  before_action :detect_device_variant
  before_action :get_top_categories

  private

  def detect_device_variant
    request.variant = :phone if browser.device.mobile? || browser.device.tablet?
  end

  def current_customer
    if customer_id = session[:customer_id]
      @current_customer ||= Customer.find customer_id
    elsif customer_id = cookies.signed[:customer_id]
      customer = Customer.find customer_id
      if customer && customer.remember_token == cookies.signed[:remember_token]
        session[:customer_id] = customer.id
        @current_customer = customer
      end
    end
  end
  helper_method :current_customer

  def current_cart
    @current_cart ||= if current_customer
                        current_customer.cart_items
                      else
                        []
                      end
  end
  helper_method :current_cart

  def current_customer_exists?
    return unless current_customer.present?
    redirect_to frontend_root_path && return
  end

  def get_top_categories
    @categories = Category.get_categories
  end
end
