class Frontend::AddressesController < Frontend::ApplicationController
  before_action :current_customer_exists?

  def index
    @addresses = current_customer.addresses
    @address   = Address.new
  end

  def create
    @address = current_customer.addresses.build(address_params)
    if @address.save
      flash[:notice] = '新建成功'
    else
      flash[:notice] = @address.errors.full_messages.join(' 。')
    end
    redirect_to frontend_addresses_url
  end

  def edit
    @addresses = current_customer.addresses
    @address   = current_customer.addresses.find(params[:id])
    render :index
  end

  def update
    @address = current_customer.addresses.find(params[:id])
    if @address.update(address_params)
      flash[:notice] = '已更新成功'
    else
      flash[:notice] = @address.errors.full_messages.join(' 。')
    end
    respond_to do |format|
      format.html { redirect_to frontend_addresses_url }
      format.js
    end
  end

  def destroy
    @address = current_customer.addresses.find(params[:id])
    @address.destroy
    redirect_to frontend_addresses_url
  end

  private

  def address_params
    params.require(:address).permit!
  end
end
