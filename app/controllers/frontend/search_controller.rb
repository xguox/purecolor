class Frontend::SearchController < Frontend::ApplicationController
  after_action -> { Suggestion.save_word(params[:q]) }, only: [:index]

  def index
    @search = Product.custom_search(params)
    @total  = @search.total_count
    @products = @search.results
  end
end
