class Frontend::SessionsController < Frontend::ApplicationController
  def new
    if current_customer
      redirect_to frontend_root_path, notice: '已经登录了.'
      return
    end
  end

  def create
    customer = Customer.find_by(email: params[:email])

    if customer && customer.authenticate(params[:password])
      session[:customer_id] = customer.id

      if params[:remember_me].present?
        customer.remember
        cookies.signed[:customer_id] = { value: customer.id, expires: 1.week.from_now }
        cookies.signed[:remember_token] = { value: customer.remember_token, expires: 1.week.from_now }
      end

      redirect_to(params[:prev_url] || frontend_root_path)
    else
      redirect_to frontend_sign_in_path, alert: '邮箱与密码不匹配'
    end
  end

  def destroy
    session.delete(:customer_id)
    cookies.delete(:customer_id)
    cookies.delete(:remember_token)
    redirect_to frontend_root_path, notice: '帐户已登出'
  end
end
