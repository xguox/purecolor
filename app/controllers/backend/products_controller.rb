class Backend::ProductsController < Backend::ApplicationController
  before_action :get_product, only: [:show, :edit, :update, :destroy, :set_cover]
  before_action -> { authorize Product }
  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to new_backend_product_option_path(@product), notice: '商品已创建成功' }
      else
        format.html { render :new }
        format.js { render :new }
      end
    end
  end

  def edit
  end

  def update
    if @product.update(product_params)
      redirect_to [:backend, @product]
    else
      render :edit
    end
  end

  def index
    @products = Product.not_deleted
  end

  def destroy
    @product.update(deleted_at: Time.now)
    redirect_to backend_products_path, notice: "#{@product.title} 已被删除"
  end

  def set_cover
    img = @product.product_images.find(params[:product_image_id])
    @product.update_column(:cover, img.img_url(:big))
  end

  private
  def product_params
    params.require(:product)
          .permit(:title, :price, :stock_qty, :status, :vendor, :body_html, :category_id, :new_arrival, :hot_sale, tag_list: [])
  end
end
