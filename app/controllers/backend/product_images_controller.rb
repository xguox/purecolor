class Backend::ProductImagesController < Backend::ApplicationController
  before_action -> { get_product(params[:product_id]) },
                only: [:index, :create, :destroy]
  before_action -> { authorize Product }

  def index
    @images = @product.product_images.order('position asc')
    @new_image = ProductImage.new
  end

  def create
    @image = @product.product_images.create(product_image_params)
  end

  def destroy
    @image = @product.product_images.find params[:id]
    @image.remove_img!
    @image.destroy
    redirect_to backend_product_product_images_path, notice: '图片已删除'
  end

  private

  def product_image_params
    params.require(:product_image).permit!
  end
end
