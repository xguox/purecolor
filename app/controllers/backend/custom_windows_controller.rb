class Backend::CustomWindowsController < Backend::ApplicationController
  before_action -> { authorize Product }

  def index
    @custom_windows = CustomWindow.order('position asc')
  end

  def new
    @custom_window = CustomWindow.new
  end

  def create
    @custom_window = CustomWindow.new custom_window_params

    if @custom_window.save
      redirect_to edit_backend_custom_window_path(@custom_window),
                  notice: '已成功创建'
    else
      render :new
    end
  end

  def edit
    @custom_window = CustomWindow.find params[:id]
  end

  def update
    @custom_window = CustomWindow.find params[:id]

    if @custom_window.update(custom_window_params)
      redirect_to edit_backend_custom_window_path, notice: '已更新'
    else
      render :new
    end
  end

  def destroy
    @custom_window = CustomWindow.find params[:id]
    @custom_window.destroy
    redirect_to backend_custom_windows_path
  end

  private

  def custom_window_params
    params.require(:custom_window).permit!
  end
end
