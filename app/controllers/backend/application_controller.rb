class Backend::ApplicationController < ActionController::Base
  include Pundit

  layout 'admin'
  protect_from_forgery with: :exception
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def user_not_authorized(exception)
    flash[:alert] = '请先登录后再继续操作.'
    redirect_to(request.referrer || backend_root_path)
  end

  def current_user
    if user_id = session[:user_id]
      @current_user ||= User.find user_id
    elsif user_id = cookies.signed[:user_id]
      user = User.find user_id
      if user && user.remember_token == cookies.signed[:remember_token]
        session[:user_id] = user.id
        @current_user = user
      end
    end
  end
  helper_method :current_user

  def get_product(product_id = nil)
    product_id ||= params[:id]
    @product = Product.find product_id
  end
end
