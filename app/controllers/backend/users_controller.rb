class Backend::UsersController < Backend::ApplicationController
  def new
    @user = User.new(user_params)
  end

  def create
    # @user = User.new(user_params)

    # if @user.save
    #   session[:user_id] = @user.id
    #   redirect_to backend_root_path
    # else
    #   flash[:notice] = @user.errors.full_messages.join(' 。')
    #   redirect_to new_backend_user_path(user: {name: user_params[:name], email: user_params[:email]})
    # end
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :name) if params[:user]
  end
end
