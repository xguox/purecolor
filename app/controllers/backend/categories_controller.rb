class Backend::CategoriesController < Backend::ApplicationController

  def index
    authorize Category
    @categories = Category.where(ancestry_depth: 0)
  end

  def new
    authorize Category
  end

  def create
    @category = Category.new(category_params)
    authorize @category
    if @category.save
      redirect_to backend_categories_url
    else
      flash[:notice] = @category.errors.full_messages.join(' 。')
      render :new
    end
  end

  def edit
    @category = Category.find(params[:id])
    authorize @category
  end

  def update
    @category = Category.find(params[:id])
    authorize @category
    if @category.update_attributes(category_params)
      redirect_to backend_categories_url
    else
      flash[:notice] = @category.errors.full_messages.join(' 。')
      render :edit
    end
  end

  def destroy
    @category = Category.find(params[:id])
    authorize @category
    if @category.present?
      @category.destroy
      @category.descendants.destroy_all
      redirect_to backend_categories_url
    end
  end

  private

  def category_params
    params.require(:category).permit!
  end
end
