class Backend::VariantsController < Backend::ApplicationController
  before_action -> {get_product(params[:product_id])}, only: [:index, :edit, :update]
  before_action -> {authorize Product}

  def index
    @options  = @product.options
    @variants = @product.variants.not_deleted
  end

  def edit
    @options = @product.options
    @variant = @product.variants.find(params[:id])
  end

  def update
    @variant = @product.variants.find(params[:id])

    if @variant.update(variant_params)
      redirect_to backend_product_variants_path
    else
      @options = @product.options
      render :edit
    end
  end
  #
  # def new
  #   @product = Product.find(params[:product_id])
  #   @options = @product.options
  #   @variant = @product.variants.build
  # end
  #
  # def create
  #   @product = Product.find(params[:product_id])
  #   @variant  = @product.variants.build(variant_params)
  #   if @variant.save
  #     redirect_to backend_product_variants_path
  #   else
  #     @options = @product.options
  #     render :new
  #   end
  # end

  private

  def variant_params
    params.require(:variant).permit!
  end
end
