class Backend::PicturesController < Backend::ApplicationController
  def create
    @pic = Picture.new(pic: params['file'])
    respond_to do |format|
      if @pic.save
        format.html do
          render  json:  { name: params['file'].original_filename, href: @pic.pic.url, type: 'image' },
                  content_type: 'text/html',
                  layout: false
        end
      end
    end
  end
end