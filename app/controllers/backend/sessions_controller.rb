class Backend::SessionsController < Backend::ApplicationController
  def new
    if current_user
      redirect_to backend_root_path, notice: 'Already login.'
      return
    end
  end

  def create
    user = User.find_by(email: params[:email])

    if user && user.authenticate(params[:password])
      session[:user_id] = user.id

      if params[:remember_me].present?
        user.remember
        cookies.signed[:user_id] = { value: user.id, expires: 1.week.from_now }
        cookies.signed[:remember_token] = { value: user.remember_token, expires: 1.week.from_now }
      end

      redirect_to backend_root_path
    else
      redirect_to backend_sign_in_path, alert: '邮箱与密码不匹配'
    end
  end

  def destroy
    session.delete(:user_id)
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
    redirect_to backend_root_path, notice: '帐户已登出'
  end
end
