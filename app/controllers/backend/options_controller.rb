class Backend::OptionsController < Backend::ApplicationController
  before_action -> { get_product(params[:product_id]) },
                only: [:new, :create, :index, :edit, :update, :update_all]
  before_action -> { authorize Product }

  def new
    if @product.options.present?
      redirect_to backend_product_options_path(@product)
      return
    end
    @options = [@product.options.build]
  end

  def create
    @options = @product.options.build(options_params['options'])
    names = @options.map(&:name)

    if @options.all?(&:valid?) && names.uniq.length == names.length
      @options.each_with_index { |option, index| option.position = index + 1 }
      ActiveRecord::Base.transaction do
        @options.each(&:save)
      end
      VariantJob.perform_later(params[:product_id])
      redirect_to backend_product_options_path(@product),
                  notice: "已成功添加 #{@options.count} 个商品选项"
    else
      flash[:error] = '选项名称不能重复' if names.uniq.length != names.length
      render :new
    end
  end

  def index
    @options = @product.options
  end

  def edit
    @option = @product.options.find params[:id]
  end

  def update
    @option = @product.options.find params[:id]
    old_values = @option.values
    new_values = single_option_params['values']

    removed = old_values - new_values
    added   = new_values - old_values

    if @option.update(single_option_params)
      SingleOptionUpdateJob.perform_later(removed, added, @option.id)
      redirect_to edit_backend_product_option_path(@product, @option),
                  notice: "选项 #{@option.name} 已更新成功"
    else
      render :edit
    end
  end

  def update_all
    current_options = @product.options
    coming_options  = options_params['options']

    current_option_names      = current_options.map(&:name)
    coming_option_options     = coming_options.map { |option| option['name'] }

    deleted_options = current_option_names - coming_option_options
    fresh_options   = coming_option_options - current_option_names

    if deleted_options.present?
      current_options.where(name: deleted_options).destroy_all
      @product.options.reload.each_with_index { |option, index| option.position = index + 1 }
      @product.options.each(&:save)
    end

    if fresh_options.present?
      @product.options.reload
      options_to_add = coming_options.select { |option| option[:name].in?(fresh_options) }
      @options = @product.options.build(options_to_add)
      @product.options.each_with_index { |option, index| option.position = index + 1 }

      ActiveRecord::Base.transaction do
        @product.options.each(&:save)
      end
    end

    if deleted_options.present? || fresh_options.present?
      @product.variants.update_all(deleted_at: Time.now) # or destroy_all ?
      VariantJob.perform_later(params[:product_id])
    end

    # 不管新的是否符合全都 redirect
    redirect_to backend_product_options_path(@product),
                notice: '商品选项已更新'
  end

  private

  def options_params
    # params.require(:options).map(&:permit!)
    params.require(:options)
    params.permit(options: [:name, :position, values: []])
    # https://github.com/rails/strong_parameters/issues/140#issuecomment-21631515
  end

  def single_option_params
    params.require(:option).permit!
  end
end
