# encoding: utf-8

class ProductImageUploader < BaseUploader
  version :thumb do
    process resize_to_fit: [100, 100]
  end

  version :large do
    process resize_to_fit: [300, 300]
  end

  version :big do
    process resize_to_fit: [500, 500]
  end
end
